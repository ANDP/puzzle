package com.personal.puzzle;

import java.util.function.IntBinaryOperator;

public class Sudoku {
    private final int[][] question;
    private final int[] horizontal;
    private final int[] vertical;
    private final int[][] box;
    private final int[][] solution;

    IntBinaryOperator visit = (n, pos) -> n | (1 << pos);
    IntBinaryOperator noVisit = (n, pos) -> n & (~(1 << pos));

    public Sudoku(int[][] sudoku) {
        this.question = sudoku;
        solution = new int[9][9];
        horizontal = new int[9];
        vertical = new int[9];
        box = new int[3][3];
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                if (sudoku[i][j] != 0)
                    setValue(sudoku[i][j], i, j);
            }
        }
    }

    void setVisitStatus(int i, int j, IntBinaryOperator setStatus) {
        horizontal[i] = setStatus.applyAsInt(horizontal[i], solution[i][j]);
        vertical[j] = setStatus.applyAsInt(vertical[j], solution[i][j]);
        int x = i / 3;
        int y = j / 3;
        box[x][y] = setStatus.applyAsInt(box[x][y], solution[i][j]);
    }

    boolean isVisited(int n, int i, int j) {
        int x = i / 3;
        int y = j / 3;
        return ((horizontal[i] >> n & 1) == 1) || ((vertical[j] >> n & 1) == 1) || ((box[x][y] >> n & 1) == 1);
    }

    void setValue(int n, int i, int j) {
        solution[i][j] = n;
        setVisitStatus(i, j, visit);
    }

    void unsetValue(int i, int j) {
        setVisitStatus(i, j, noVisit);
        solution[i][j] = 0;
    }

    boolean solve() {
        return backtrack(0);
    }

    boolean backtrack(int curNo) {
        if (curNo == 81) {
            return true;
        }

        int x = curNo / 9;
        int y = curNo % 9;

        if (solution[x][y] != 0) {
            return backtrack(curNo + 1);
        }
        for (int i = 1; i <= 9; ++i) {
            if (!isVisited(i, x, y)) {
                setValue(i, x, y);
                if (backtrack(curNo + 1)) return true;
                unsetValue(x, y);
            }
        }

        return false;
    }


    void prettyPrintSolution() {
        prettyPrint(solution);
    }


    void prettyPrintQuestion() {
        prettyPrint(question);
    }

    void prettyPrint(int[][] sudoku) {
        System.out.println(" _______________________");
        for (int i = 0; i < 9; ++i) {
            System.out.print("| ");
            for (int j = 0; j < 9; ++j) {
                String delim = (j + 1) % 3 == 0 ? " | " : " ";
                System.out.print(sudoku[i][j] + delim);
            }
            System.out.println();
            if ((i + 1) % 3 == 0) {
                System.out.println("|_______________________|");
            }
        }
    }

    public static void main(String[] args) {
        Sudoku sudoku = new Sudoku(
                new int[][]{
                        {0, 0, 0, 0, 0, 5, 2, 0, 0},
                        {0, 8, 9, 0, 2, 0, 0, 3, 0},
                        {0, 6, 0, 0, 0, 0, 0, 0, 0},
                        {0, 2, 3, 0, 5, 0, 0, 8, 0},
                        {0, 0, 0, 4, 0, 0, 0, 0, 7},
                        {1, 0, 0, 0, 0, 0, 0, 0, 0},
                        {6, 0, 0, 0, 9, 0, 0, 0, 0},
                        {5, 0, 0, 0, 0, 0, 0, 1, 0},
                        {0, 9, 1, 0, 0, 7, 0, 0, 8}
                }
        );

        if (sudoku.solve()) {
            sudoku.prettyPrintSolution();
        } else {
            System.out.println("Invalid sudoku");
            sudoku.prettyPrintQuestion();
        }

    }

}